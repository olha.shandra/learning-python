# class BlogPost():
#     def __init__(self, user_name, text, number_of_likes):
#         self.user_name = user_name
#         self.text = text
#         self.number_of_likes = number_of_likes

# insta = BlogPost('olko','Hello cat', 54)
# twitter = BlogPost('Mysha', 'Hello human', 54)

# twitter.number_of_likes = 99


# print(insta.number_of_likes)
# print(twitter.number_of_likes)


# # #  с атрибутами user_name, text, number_of_likes. 
# # # Создайте два объекта этого класса. 
# # # После создания измените атрибут number_of_likes одного из объектов. 
# # # Распечатайте атрибут number_of_likes каждого из объектов


# class BankAccount():

#     def __init__(self, client_id, client_first_name, client_last_name, balance = 0.0):
#         self.client_id = client_id
#         self.client_first_name = client_first_name
#         self.client_last_name = client_last_name
#         self.balance = balance

#     def add(self, income):
#         self.balance += income

#     def withdraw(self, spending):
#         self.balance -= spending
   

# b1 = BankAccount (1, 'Olha', 'Shandra')
# b2 = BankAccount (2, 'Mykola', 'Denysiuk')

# b1.add(100)
# b1.withdraw(51)
# b1.add(5)

# print(b1.balance)


# class BankAccount:

#     def __init__(self, client_id, client_first_name,
#                  client_last_name):
#         self.client_id = client_id
#         self.client_first_name = client_first_name
#         self.client_last_name = client_last_name
#         self.balance = 0.0

#     def add(self, amount):
#         self.balance += amount

#     def withdraw(self, amount):
#         self.balance -= amount


# account_1 = BankAccount(1, 'John', 'Brown')
# account_2 = BankAccount(2, 'Jim', 'White')

# account_1.add(1000)
# print(account_1.balance)
# account_1.withdraw(500)
# print(account_1.balance)
# print(account_2.balance)


# Создайте класс BankAccount с атрибутами client_id, client_first_name, client_last_name, balance 
# и методами add() и withdraw(), при помощи которых можно пополнять счет 
# и выводить средства со счета соответственно. 
# Атрибут balance должен инициализироваться по умолчанию значением 0.0 
# и меняться при срабатывании методов add() и withdraw().



# class GameCharacter:
#     def __init__(self, name, health, level):
#         self.name = name
#         self.health = health
#         self.level = level

#     def speak(self):
#         print('Hi, my name is ' + self.name)

 
# class Villain(GameCharacter):
#     def __init__(self, name, health, level):
#         GameCharacter.__init__(self, name, health, level)


#     def speak(self):
#         print('Hi, my name is ' + self.name  + ' and I will kill you')


#     def kill (self, other):
#         other.health = 0
#         print('Bang-bang, now you\'re dead')

# game_1 = Villain('tom', 10, 3)
# game_2 = GameCharacter ('jerry', 10, 2)

# game_1.speak()
# print(game_2.health)

# game_1.kill(game_2)
# print(game_2.health)

# методом kill(), который принимает в качестве параметра объект класса 
# GameCharacter, присваивает атрибуту health этого объекта значение 
# 0 и  выводит на печать 'Bang-bang, now you're dead'



from gc import get_objects


class Chain:
    def __init__(self, number_of_items):
        self.number_of_items = number_of_items

    def info(self):
        print(self.number_of_items)

    def __str__(self):
        return f"Chain with {self.number_of_items} items"

    def __len__ (self):
        return self.number_of_items

neckless = Chain(45)



print(neckless)
neckless.info()

