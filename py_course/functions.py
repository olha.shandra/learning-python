# def my_function(food):
#     for x in food:
#         print(x)
#
# fruits = ["apple", "banana", "cherry"]
#
# my_function(fruits)

# def my_function(x):
#     return 5 * x
#
# print(my_function(3))


# 1 Создайте функции cat_voice() and dog_voice(), которые выводят на экран 'Meow!' и 'Woof!' соответственно. Сделайте по одному вызову каждой из функций
def cat_voice():
    print('Meow!')


def dog_voice():
    print('Woof!')


cat_voice()
dog_voice()


# 2 Создайте функции cat_voice() and dog_voice(), которые возвращают значения 'Meow!' и 'Woof!' соответственно. Выведите на экран 'Meow!' и 'Woof!' по 2 раза

def cat_voice(x):
    return 2 * x


def dog_voice(x):
    return 2 * x


# # def dog_voice():
# #     return 'Woof!'
# print(dog_voice())

print(cat_voice('Meow! '))
print(dog_voice('Woof! '))


# 3 Создайте функцию get_voice() которая возвращает передаваемый ей в качестве параметра текст c восклицательным знаком.
def get_voice(*text):
    print(text[1] + '!')


get_voice('Hello', 'Hice to see you')


# def get_voice(voice):
#     return str(voice) + '!'


# 4 Создайте функцию, которая генерирует последовательность нечетных чисел в диапазоне от a до b (a и b включительно). Значения a и b должны передаваться в качестве параметров. Результирующая последовательность должна возвращаться в форме объекта list. Решите задание двумя способами - при помощи List Comprehension  и без него
# def odd_numbers_list(a,b):


def get_odd_number_list(a, b):
    number_list = list(range(a, b + 1))
    odd_number_list = []
    for number in number_list:
        if number % 2 == 1:
            odd_number_list.append(number)
    return odd_number_list


print(get_odd_number_list(1, 30))


def get_even_number_list(a, b):
    number_list = list(range(a, b + 1))
    even_number_list = []
    for number in number_list:
        if number % 2 == 0:
            even_number_list.append(number)
    return(even_number_list)
print(get_even_number_list(1, 30))


def get_odd_number_list(a, b):
    number_list = list(range(a, b + 1))
    odd_number_list = [number for number in number_list if number % 2 == 1]
    return odd_number_list


print(get_odd_number_list(1, 30))
 



