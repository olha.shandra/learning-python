# def  function_with_args_and_kwargs(*args, **kwargs):
#     print('I would like {} {}'.format(args[2], kwargs['food']))

# function_with_args_and_kwargs('1','2', '3', '4', drink ='coffee', food = 'sandwiches')

# Создайте функцию is_cat_here(), которая принимает любое количество аргументов и проверяет 
# есть ли строка 'cat' среди них. Функция должна возвращать True, если такой параметр есть 
# и False в обратном случае. Буквы в строке 'cat' могут быть как большие, так и маленькие
def is_cat_here(text):
    return 'cat' in text.lower()
    # if 'cat' in text.lower():
    #     return True
    # else:
    #     return False
print(is_cat_here('Hello my dear Cat! how are you doing?'))


# why False if there's 'cat' in one of the parameters?
def is_cat_here(*args):
    args_in_lower_case = [str(args).lower() for arg in list(args)]
    if 'cat' in args_in_lower_case:
        return True
    else:
        return False

print(is_cat_here('fff', 'Hello my dear cat! how are you doing?', 'Hello people!'))

     

# Создайте функцию is_item_here(item, *args), которая проверяет есть ли  item среди args. 
# Функция должна возвращать True, если такой параметр есть и False в обратном случае.
def is_item_here(item, *args):
    if item in args:
        return True
    else:
        return False


print(is_item_here('pizza', 'apple', 'banana', 'kiwi')) 



# Создайте функцию your_favorite_color() с позиционным параметром my_color и параметрами 
# **kwargs, которая будет выводить на экран 'My favorite color is (значение my_color), 
# what is your favorite color?', если в параметрах kwargs нет ключа color, 
# и 'My favorite color is (значение my_color), but (значение по ключу color) is also pretty good!',
#  если в параметрах kwargs ключ color присутствует
def your_favorite_color(my_color, **kwargs):
    if 'color' not in kwargs:
        print('My favorite color is {}. What is your favorite color?'.format(my_color))
    else:
        print('My favorite color is {}, but {} is also pretty good!'.format(my_color, kwargs['color']))

your_favorite_color('green', color='yellow')

