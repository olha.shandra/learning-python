def reverse_words(text):
    return " ".join(word[::-1] for word in text.split(" "))
    
    # reverse_str = ""
    # text = text.split()
    # for i in text:
    #     reverse_str = reverse_str + i[::-1]+" "
    
    # return reverse_str



# print(reverse_words('The quick brown fox jumps over the lazy dog.'))


def friend(x):
    # friend_list = []
    # for i in x:
    #     if len(i) == 4:
    #         friend_list.append(i)
    # return friend_list

    return [i for i in x if len(i) == 4]

# print(friend(["Ryan", "Kieran", "Jason", "Yous"]))
        

def remove_exclamation_marks(s):
    # clean_str = ""
    # for i in s:
    #     if i != '!':
    #         clean_str = clean_str + i
    # return clean_str

    # return "".join(i for i in s if i != '!')
    return s.replace('!', '')


# print(remove_exclamation_marks("Hello Wor!ld!"))

def disemvowel(string):
    no_vowel_str = ""
    # vowels = 'a' 'o' 'i' 'u' 'e' 'A' 'O' 'I' 'U' 'E'
    for i in string:
        if i not in "aeiouAEIOU":
            no_vowel_str = no_vowel_str + i
    return no_vowel_str


def disemvowel(string):
   return string.translate(None, "aeiouAEIOU")


# The translate() method returns a string where some specified characters are replaced with the character described in a dictionary, or in a mapping table.
# print(disemvowel("This website is for losers LOL!"))


def repeat_str(repeat, string):
    # repeated_str = ""
    return ''.join(string * repeat)
    # return repeat * string
# print(repeat_str(2, 'I'))


def we_rate_dogs(string, rating):
    string = string.split()
    correct_rating = ""
    for i in string:
        if '/' in i:
            correct_rating = correct_rating + " " + f'{rating}/10'
        else:
            correct_rating = correct_rating + " " + f'{i}'
    return correct_rating[1:]

    # return "".join(w.replace(w, f'{rating}/10') for w in string.split(" ") if '/' in w)

# print(we_rate_dogs('This is Tucker. He would like a hug. 3/10 someone hug him', 11))
