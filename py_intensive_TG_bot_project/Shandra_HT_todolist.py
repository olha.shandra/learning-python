to_do_list = {
    'Feb 16': ['attend a webinar', 'complete home tasks'],
    'Feb 17': ['meet for pizza', 'evening class', 'party'],
    'Feb 18': ['order cat\'s food', 'fencing training']
}

# import pprint
# pprint.pprint(to_do_list)
# print(to_do_list)

# # adding a new date and tasks to the dict
# to_do_list['Feb 19'] = ['buy a B-day cake', ' cook some food']

# # updating the existing date
# to_do_list.update({'Feb 16': ['do nothing']})

# # adding a new task to the list
# to_do_list['Feb 17'].append('do nothing')

# # deleting the 1st item from the list
# to_do_list['Feb 18'].pop(0)


# pprint.pprint(to_do_list)


# while cycle to ask for a new task
while True:
    answer = input('What would you like to do:  \n'
                   'to add a new date and task, type "new":  \n'
                   'to delete all tasks from the date, type "delete":  \n'
                   'if you don\'t need to do anything, type "stop": ')

    if answer == "delete":
        date = input('Please, type in the date like this "mm-dd-yyyy" ')
        if date not in to_do_list:
            print('This date is not on your To_do_list. Please, try again.')
            continue
        elif date in to_do_list:
            confirm = input('Are you sure you want to delete it?')
            if confirm == 'yes':
                to_do_list[date].pop()
                print(date + ' was successfully deleted from your To_do_list.')
            else:
                print(date + ' was not deleted from your To_do_list.')
            continue
    # elif answer == 'add':
    #     date = input('Please, type in the date like this "mm-dd-yyyy" ')
    #     if date not in to_do_list:
    #         print('This date is not on your To_do_list. Please, try again.')
    #         continue
    #     elif date in to_do_list:
    #         new_task = input('Please, type in the task.')
    #         if new_task == '':
    #             print('You have to write the task you need to do')
    #             continue
    #         else:
    #             to_do_list[date].append(new_task)
    #             print('A new task was successfully added to ' + date)
    #         continue

    elif answer == 'new':
        new_date = input('Please, type in the date like this "mm-dd-yyyy": ')
        new_task = input('Please, type in the task: ')
        if new_date in to_do_list:
            to_do_list[new_date].append(new_task)
        else:
            to_do_list[new_date] = [new_task, ]
            print('Thank you! The task is on your To_do_list. ')

    elif answer == "stop":
        print('Thank you! Bye.')
        break


    for date in to_do_list:
        print(f"Tasks for  {date}:")
        num_task = 1
        for task in to_do_list[date]:
            print(num_task, task)
            num_task += 1
        print()